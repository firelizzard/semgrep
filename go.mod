module gitlab.com/gitlab-org/security-products/analyzers/semgrep

go 1.15

require (
	github.com/Microsoft/go-winio v0.6.0 // indirect
	github.com/ProtonMail/go-crypto v0.0.0-20220930113650-c6815a8c17ad // indirect
	github.com/cloudflare/circl v1.2.0 // indirect
	github.com/emirpasic/gods v1.18.1 // indirect
	github.com/imdario/mergo v0.3.13 // indirect
	github.com/kevinburke/ssh_config v1.2.0 // indirect
	github.com/pelletier/go-toml v1.9.5 // indirect
	github.com/sirupsen/logrus v1.9.0
	github.com/stretchr/testify v1.8.1
	github.com/urfave/cli/v2 v2.23.7
	gitlab.com/gitlab-org/security-products/analyzers/command v1.10.1
	gitlab.com/gitlab-org/security-products/analyzers/common/v3 v3.2.2
	gitlab.com/gitlab-org/security-products/analyzers/report/v3 v3.17.0
	gitlab.com/gitlab-org/security-products/analyzers/ruleset v1.4.0
	golang.org/x/crypto v0.5.0
	gopkg.in/yaml.v2 v2.4.0
)
